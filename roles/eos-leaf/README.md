# Ansible Role: eos-leaf

This role configures leaf switches 

## Requirements

python3-netaddr (can also be installed with `pip3 install netaddr`)

## Role Variables

### BGP Variables
`leaf_asn` and `spine_asn` are set in the site-specific fabric group, such as `c99_fabric`.  
e.g. `spine_asn: 65000` and `leaf_asn: 65001`

`router_id` is typically set in the host_vars file and relies on the `loopback0.ip` value  
e.g. `router_id: "{{loopback0.ip | ipaddr('address')}}"`

`route_servers` is a list of the route server IPs in the datacenter and is set in the site-specific fabric group, such as `c99_fabric`  
BGP neighbours in the EVPN address-family will be set up to these IP addresses.
e.g.
```
route_servers:
  - 10.168.63.0
  - 10.168.63.1
  - 10.168.63.2
```

`bgp_password` is used for bgp authentication.

`mlag_linknet` is used both to set addresses on the MLAG interfaces on leaves and to establish BGP neighbor relationships between MLAG partners.  
Control-plane ACLs also utilize this value to permit BGP from appropriate IP ranges.  
It is stored in the mlag partner-group, named something like $site\_sw\_$pod\_$speed\_$number - examples include `c99_sw_f_1g_01` or `c99_sw_f_10g_01`.  

e.g. `mlag_linknet: 10.168.63.200/31`

`loopback0` and `loopback1` are used for EVPN peering and VTEP services respectively, and are typically set in host_vars. `loopback1` is required to be identical between mlag partners.  
e.g.
```
loopback0:
  ip: 10.168.63.72/32
loopback1:
  ip: 10.168.63.132/32
```

`uplinks` is a list of connections from the leaf to the spines. Underlay links are defined and BGP neighbors created based on these entries.  
e.g.
```
uplinks:
  - { interface: ethernet49/1, linknet: 10.168.60.16/31, neighbor: test-c99-j0101-sc01, neighbor_int: eth31/1 }
  - { interface: ethernet50/1, linknet: 10.168.61.16/31, neighbor: test-c99-d0101-sc01, neighbor_int: eth31/1 }
  - { interface: ethernet51/1, linknet: 10.168.62.16/31, neighbor: test-c99-k0101-sc01, neighbor_int: eth31/1 }
```

### VLAN/VXLAN variables
`site_vlans` is a list of all the vlans in the site. It's typically set in the site-specific vlan group, such as `c99_vlans`  
The subproperties of the entries are used to  
 - create the vlans on the switch (`vlan_name`, `vlan_id`)
 - associate the vlan to a VNI (`vlan_id` + 10000)
 - associate the vlan to a handover (if a tag on a vlan matches a handover definition tag, that vlan will be added to the handover)  
e.g.
```
site_vlans:
  - { vlan_name: test, vlan_id: 276, tags: ['pe01'] }
  - { vlan_name: test, vlan_id: 277, tags: ['test_mgmt'] }
```

### Handovers

These are defined in host_vars for single-link handovers, and in group_vars for multi-chassis (mlag) handovers.

**MLAG handovers**

`mlag_handovers` definitions will create a port-channel on an mlag switch pair.  
This should be defined in the mlag partner-group, named something like $site\_sw\_$pod\_$speed\_$number - examples include `c99_sw_f_1g_01` or `c00_sw_f_10g_01`.  
By default it will create a simple trunk that passes no vlans.

The subvalues `type`, `members`, `description`, `portchannel`, `tags`, `native`, `state` & `xlate` provide for customization.
 - `type` can be 'client', 'service_provider' or 'infrastructure'. 
   - infrastructure: sflow disabled, lldp enabled.
   - client & service_provider: sflow enabled, lldp disabled.
 - `members` is a list of the port-channel member interfaces.
 - `description` is whatever description you want on this handover (will also be applied to port-channel members)
 - `portchannel` is the port-channel ID to be used for this handover. Valid ranges are 1-1999.
 - `tags` is a list of tags. If these match a tag on an entry in `site_vlans` the vlan will be added to the trunk.
 - `native` is optional. It should be a tag that matches only one entry in `site_vlans`. It will be set as the trunk's 'native' vlan and added to the trunk.
 - `state` is optional. When set, the interface will be configured with its value, such as 'shutdown' or 'no shutdown'. By default this is set to 'no shutdown'.
 - `xlate` is optional and sets up vlan translation on 7060-48YC6 leaves. It should be a dictionary with src: and dst: values. Example below. 

e.g.
```
mlag_handovers:
  - compute_equipment_name:
      type: infrastructure
      members:
        - Ethernet8
      description: "compute handover"
      portchannel: 6
      tags: 
        - all

  - storage_array:
      type: infrastructure
      members:
        - Ethernet39
      description: "some type of NAS"
      portchannel: 39
      tags:
        - storage
	- storage_specific_application_tag
      state: shutdown

  - client_name:
      type: client
      members:
        - Ethernet43
      description: "client handover (ID# 0000)"
      portchannel: 43
      tags: 
        - client_name_tagdesc
      xlate:
        - {src: 100, dst: 200}
        - {src: 101, dst: 280}
```

**Single Handovers**

`single_handovers` definitions will create a trunk interface on a single switch.  
By default it will create a simple trunk that passes no vlans.  
The subvalues `type`, `interface`, `description`, `tags`, `native`, `state` & `xlate` provide for customization.  
 - `type` can be 'client', 'service_provider' or 'infrastructure'.
   - 'infrastructure': sflow disabled, lldp enabled, bpduguard enabled, bpdufilter disabled.
   - 'client' & 'service_provider': sflow enabled, lldp disabled, bpduguard enabled, bpduilter disabled.
 - `interface` is the handover interface.
 - `description` is whatever description you want on this handover
 - `tags` is a list of tags. If these match a tag on an entry in `site_vlans` the vlan will be added to the trunk.
 - `native` is optional. It should be a tag that matches only one entry in site_vlans. It will be set as the trunk's 'native' vlan and added to the trunk.
 - `state` is optional. When set, the interface will be configured with its value, such as 'shutdown' or 'no shutdown'. By default this is set to 'no shutdown'.
 - `xlate` is optional and sets up vlan translation on 7060-48YC6 leaves. It should be a dictionary with src: and dst: values. Example below. 
e.g.
```
  - client_appliance_colo:
      type: client
      interface: Ethernet20
      description: "Client somethingdescriptive (ID# 0001)"
      native: client_appliance
      tags:
        - client_appliance

  - dmz_fw:
      type: infrastructure
      interface: Ethernet13
      description: "DMZ FW"
      native: fw_dmz
      tags:
        - fw_dmz

  - client_ha_link:
      type: client
      interface: Ethernet14
      description: "client haproxy failover"
      native: clint_hap
      tags:
        - client_hap
      xlate:
        - {src: 100, dst: 200}
```

## Tasks

### main.yml

This task sets up the folder for configuration fragments to be placed in, assembles them into a single configuration file and pushes them to the switch.  
The eos-common role is included in this playbook.  
Once configuration has been pushed, it saves the running-config to startup-config.  
When this task has executed it will delete the configuration folder, fragments and the final configuration file for the host it executed against.

